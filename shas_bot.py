from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, InlineQueryHandler, \
    CallbackQueryHandler, ConversationHandler
from telegram import InlineQueryResultArticle, InputTextMessageContent, \
    KeyboardButton, ReplyKeyboardMarkup, ReplyKeyboardRemove, ChatAction, ParseMode, InlineKeyboardButton, \
    InlineKeyboardMarkup
from functools import wraps
import logging
import links_to_files
# "https://ancient-ridge-96866.herokuapp.com/ | https://git.heroku.com/ancient-ridge-96866.git"

# def send_action(action):
#     """Sends `action` while processing func command."""
#
#     def decorator(func):
#         @wraps(func)
#         def command_func(*args, **kwargs):
#             bot, update = args
#             bot.send_chat_action(chat_id=update.message.chat_id, action=action)
#             func(bot, update, **kwargs)
#
#         return command_func
#
#     return decorator


# send_typing_action = send_action(ChatAction.TYPING)
# send_audio_action = send_action(ChatAction.UPLOAD_AUDIO)
# send_document_action = send_action(ChatAction.UPLOAD_DOCUMENT)

pereki_kodsim = ["זבחים", "מנחות"]# links_to_files.the_daf_dict_.keys()

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
logger = logging.getLogger(__name__)
TOKEN = "715563653:AAFnn4Q9VbH8NlLEo2jLYI6qRe8vfextg0o"
URL = f"https://api.telegram.org/bot{TOKEN}/"


def start(bot, update):
    # keyboard = links_to_files.get_seder()

    bot.send_message(chat_id=update.message.chat_id, text=f"שלום {update.message.from_user.first_name}\n"
                                                          f"ברוך הבא לפרוייקט תמצית דף היומי!")

    update.message.reply_text(f"באפשרותך לשלוח לבקש מסכת ודף בטקסט חופשי או לבחור בכפתורים למטה את הסדר והמסכת. ")
    update.message.reply_text(links_to_files.sedr_menu_message(),
                              reply_markup=links_to_files.get_seder())
    # reply_markup = ReplyKeyboardMarkup(keyboard, one_time_keyboard=True)
    # update.message.reply_text('התחל:')


def echo(bot, update):

    my_message = update.message.text
    try:
        my_links = links_to_files.get_my_daf_links(str(my_message).strip("@TheShasBot").strip())
        # print(my_links)
        # print("*****************")
        # print(my_links[0]["pdf"])
        # print(my_links[0]["audio"])
        update.message.reply_audio(my_links[0]["audio"], quote=True)
        update.message.reply_document(my_links[0]["pdf"], quote=True)
        # update.message.reply_text(f"  ")
    except :
        update.message.reply_text(f"!לא מצאתי את מה שביקשת")
        update.message.reply_text(f"אנא וודא שכתבת את הבקשה כראוי:")
        # update.message.reply_text(f"_מסכת_ דף _מספר הדף_ ")
        bot.send_message(chat_id=update.effective_message.chat_id,
                         text=f"_מסכת_ דף  _מספר הדף_ ",
                         parse_mode=ParseMode.MARKDOWN)
        bot.send_message(chat_id=update.effective_message.chat_id,
                         text=f"*לדוגמא:*  _זבחים_ `דף`  _יא_",
                         parse_mode=ParseMode.MARKDOWN)
        update.message.reply_text(f"הרובט באמצע בניה יתכן ולא קיים כל המסכתות...")
    # update.message.reply_text(f"your message: {my_message}")
    # bot.send_message(chat_id=update.effective_message.chat_id,
    #                  text="*bold* _italic_ `fixed width font` [link](http://google.com).",
    #                  parse_mode=ParseMode.MARKDOWN)


def unknown(bot, update):
    bot.send_message(chat_id=update.message.chat_id, text=f"Sorry {update.message.from_user.first_name},"
                                                          f" \n I didn't understand the command: {update.message.text}.")


def button(bot, update):
    query = update.callback_query

    bot.edit_message_text(text="Selected option: {}".format(query.data),
                          chat_id=query.message.chat_id,
                          message_id=query.message.message_id)


def error(bot, update, my_error):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, my_error)


def main_menu(bot, update):
    query = update.callback_query
    bot.edit_message_text(chat_id=query.message.chat_id,
                          message_id=query.message.message_id,
                          text=links_to_files.sedr_menu_message(),
                          reply_markup=links_to_files.get_seder())


def zeraim_menu(bot, update):
    query = update.callback_query
    # print(dir(update.callback_query))
    # print(update.callback_query)
    # print(update.callback_query["data"])
    # print(update.callback_query.message["text"])
    bot.edit_message_text(chat_id=query.message.chat_id,
                          message_id=query.message.message_id,
                          text=links_to_files.masechet_menu_message(),
                          reply_markup=links_to_files.zeraim_list())


def moeid_menu(bot, update):
    query = update.callback_query
    bot.edit_message_text(chat_id=query.message.chat_id,
                          message_id=query.message.message_id,
                          text=links_to_files.masechet_menu_message(),
                          reply_markup=links_to_files.moeid_list())


def nashim_menu(bot, update):
    query = update.callback_query
    bot.edit_message_text(chat_id=query.message.chat_id,
                          message_id=query.message.message_id,
                          text=links_to_files.masechet_menu_message(),
                          reply_markup=links_to_files.nashim_list())


def nezikin_menu(bot, update):
    query = update.callback_query
    bot.edit_message_text(chat_id=query.message.chat_id,
                          message_id=query.message.message_id,
                          text=links_to_files.masechet_menu_message(),
                          reply_markup=links_to_files.nezikin_list())


def kodshim_menu(bot, update):
    query = update.callback_query
    bot.edit_message_text(chat_id=query.message.chat_id,
                          message_id=query.message.message_id,
                          text=links_to_files.masechet_menu_message(),
                          reply_markup=links_to_files.kodshim_list())


def teharot_menu(bot, update):
    query = update.callback_query
    bot.edit_message_text(chat_id=query.message.chat_id,
                          message_id=query.message.message_id,
                          text=links_to_files.masechet_menu_message(),
                          reply_markup=links_to_files.teharot_list())


# def zvachim_dapim() -> InlineKeyboardMarkup:
#     keyboard = [
#         [InlineKeyboardButton("זבחים דף ב'", callback_data="זבחים דף ב'")],
#         [InlineKeyboardButton("זבחים דף ג'", callback_data="זבחים דף ג'"),
#          InlineKeyboardButton("זבחים דף ד'", callback_data="זבחים דף ד'")],
#         [InlineKeyboardButton("זבחים דף ה'", callback_data="זבחים דף ה'"),
#          InlineKeyboardButton("זבחים דף ו'", callback_data="זבחים דף ו'")],
#         [InlineKeyboardButton("זבחים דף ז'", callback_data="זבחים דף ז'"),
#          [InlineKeyboardButton("זבחים דף ח'", callback_data="זבחים דף ח'")]],
#         [InlineKeyboardButton("זבחים דף ט'", callback_data="זבחים דף ט'"),
#          [InlineKeyboardButton("זבחים דף י'", callback_data="זבחים דף י'")]],
#         [InlineKeyboardButton("זבחים דף יא'", callback_data="זבחים דף יא'"),
#          [InlineKeyboardButton("זבחים דף יב'", callback_data="זבחים דף יב'")]],
#         [InlineKeyboardButton("זבחים דף יג'", callback_data="זבחים דף יג'"),
#          [InlineKeyboardButton("זבחים דף יד'", callback_data="זבחים דף יד'")]],
#         [InlineKeyboardButton("זבחים דף טו'", callback_data="זבחים דף טו'"),
#          [InlineKeyboardButton("זבחים דף טז'", callback_data="זבחים דף טז'")]],
#         [InlineKeyboardButton("זבחים דף יח'", callback_data="זבחים דף יח'"),
#          [InlineKeyboardButton("זבחים דף יט'", callback_data="זבחים דף יט'")]],
#         [InlineKeyboardButton("זבחים דף כ'", callback_data="זבחים דף כ'"),
#          [InlineKeyboardButton("זבחים דף כא'", callback_data="זבחים דף כא'")]],
#         [InlineKeyboardButton("זבחים דף כב'", callback_data="זבחים דף כב'"),
#          [InlineKeyboardButton("זבחים דף כג'", callback_data="זבחים דף כג'")]],
#         [InlineKeyboardButton("זבחים דף כד'", callback_data="זבחים דף כד'"),
#          [InlineKeyboardButton("זבחים דף כה'", callback_data="זבחים דף כה'")]],
#         [InlineKeyboardButton("זבחים דף כו'", callback_data="זבחים דף כו'"),
#          [InlineKeyboardButton("זבחים דף כז'", callback_data="זבחים דף כז'")]],
#         [InlineKeyboardButton("זבחים דף כח'", callback_data="זבחים דף כח'"),
#          [InlineKeyboardButton("זבחים דף כט'", callback_data="זבחים דף כט'")]],
#         [InlineKeyboardButton("זבחים דף ל'", callback_data="זבחים דף ל'"),
#          [InlineKeyboardButton("זבחים דף לא'", callback_data="זבחים דף לא'")]],
#         [InlineKeyboardButton("זבחים דף לב'", callback_data="זבחים דף לב'"),
#          [InlineKeyboardButton("זבחים דף לג'", callback_data="זבחים דף לג'")]],
#         [InlineKeyboardButton("זבחים דף לד'", callback_data="זבחים דף לה'"),
#          [InlineKeyboardButton("זבחים דף לו'", callback_data="זבחים דף לו'")]],
#         [InlineKeyboardButton("זבחים דף לז'", callback_data="זבחים דף לז'"),
#          [InlineKeyboardButton("זבחים דף לח'", callback_data="זבחים דף לח'")]],
#         [InlineKeyboardButton("זבחים דף לט'", callback_data="זבחים דף לט'"),
#          [InlineKeyboardButton("זבחים דף מ'", callback_data="זבחים דף מ'")]],
#         [InlineKeyboardButton("זבחים דף מא'", callback_data="זבחים דף מא'"),
#          [InlineKeyboardButton("זבחים דף מב'", callback_data="זבחים דף מב'")]],
#         [InlineKeyboardButton("זבחים דף מג'", callback_data="זבחים דף מג'"),
#          [InlineKeyboardButton("זבחים דף מד'", callback_data="זבחים דף מד'")]],
#         [InlineKeyboardButton("זבחים דף מה'", callback_data="זבחים דף מה'"),
#          [InlineKeyboardButton("זבחים דף מו'", callback_data="זבחים דף מו'")]],
#         [InlineKeyboardButton("זבחים דף מז'", callback_data="זבחים דף מז'"),
#          [InlineKeyboardButton("זבחים דף מח'", callback_data="זבחים דף מח'")]],
#         [InlineKeyboardButton("זבחים דף מט'", callback_data="זבחים דף מט'"),
#          [InlineKeyboardButton("זבחים דף נ'", callback_data="זבחים דף נ'")]],
#         [InlineKeyboardButton("זבחים דף נא'", callback_data="זבחים דף נא'"),
#          [InlineKeyboardButton("זבחים דף נב'", callback_data="זבחים דף נב'")]],
#         [InlineKeyboardButton("זבחים דף נג'", callback_data="זבחים דף נג'"),
#          [InlineKeyboardButton("זבחים דף נד'", callback_data="זבחים דף נד'")]],
#         [InlineKeyboardButton("זבחים דף נה'", callback_data="זבחים דף נה'"),
#          [InlineKeyboardButton("זבחים דף נו'", callback_data="זבחים דף נו'")]],
#         [InlineKeyboardButton("זבחים דף נז'", callback_data="זבחים דף נז'"),
#          [InlineKeyboardButton("זבחים דף נח'", callback_data="זבחים דף נח'")]],
#         [InlineKeyboardButton("זבחים דף נט'", callback_data="זבחים דף נט'"),
#          [InlineKeyboardButton("זבחים דף ס'", callback_data="זבחים דף ס'")]],
#         [InlineKeyboardButton("זבחים דף סא'", callback_data="זבחים דף סא'"),
#          [InlineKeyboardButton("זבחים דף סב'", callback_data="זבחים דף סב'")]],
#         [InlineKeyboardButton("זבחים דף סג'", callback_data="זבחים דף סג'"),
#          [InlineKeyboardButton("זבחים דף סד'", callback_data="זבחים דף סד'")]],
#         [InlineKeyboardButton("זבחים דף סה'", callback_data="זבחים דף סה'"),
#          [InlineKeyboardButton("זבחים דף סו'", callback_data="זבחים דף סו'")]],
#         [InlineKeyboardButton("זבחים דף סז'", callback_data="זבחים דף סז'"),
#          [InlineKeyboardButton("זבחים דף סח'", callback_data="זבחים דף סח'")]],
#         [InlineKeyboardButton("זבחים דף סט'", callback_data="זבחים דף סט'"),
#          [InlineKeyboardButton("זבחים דף ע'", callback_data="זבחים דף ע'")]],
#         [InlineKeyboardButton("זבחים דף עא'", callback_data="זבחים דף עא'"),
#          [InlineKeyboardButton("זבחים דף עב'", callback_data="זבחים דף עב'")]],
#         [InlineKeyboardButton("זבחים דף עג'", callback_data="זבחים דף עג'"),
#          [InlineKeyboardButton("זבחים דף עד'", callback_data="זבחים דף עד'")]],
#         [InlineKeyboardButton("זבחים דף עה'", callback_data="זבחים דף עה'"),
#          [InlineKeyboardButton("זבחים דף עו'", callback_data="זבחים דף עו'")]],
#         [InlineKeyboardButton("זבחים דף עז'", callback_data="זבחים דף עז'"),
#          [InlineKeyboardButton("זבחים דף עח'", callback_data="זבחים דף עח'")]],
#         [InlineKeyboardButton("זבחים דף עט'", callback_data="זבחים דף עט'"),
#          [InlineKeyboardButton("זבחים דף פ'", callback_data="זבחים דף פ'")]],
#         [InlineKeyboardButton("זבחים דף פא'", callback_data="זבחים דף פא'"),
#          [InlineKeyboardButton("זבחים דף פב'", callback_data="זבחים דף פב'")]],
#         [InlineKeyboardButton("זבחים דף פג'", callback_data="זבחים דף פג'"),
#          [InlineKeyboardButton("זבחים דף פד'", callback_data="זבחים דף פד'")]],
#         [InlineKeyboardButton("זבחים דף פה'", callback_data="זבחים דף פה'"),
#          [InlineKeyboardButton("זבחים דף פו'", callback_data="זבחים דף פו'")]],
#         [InlineKeyboardButton("זבחים דף פז'", callback_data="זבחים דף פז'"),
#          [InlineKeyboardButton("זבחים דף פח'", callback_data="זבחים דף פח'")]],
#         [InlineKeyboardButton("זבחים דף פט'", callback_data="זבחים דף פט'"),
#          [InlineKeyboardButton("זבחים דף צ'", callback_data="זבחים דף צ'")]],
#         [InlineKeyboardButton("זבחים דף צא'", callback_data="זבחים דף צא'"),
#          [InlineKeyboardButton("זבחים דף צב'", callback_data="זבחים דף צב'")]],
#         [InlineKeyboardButton("זבחים דף צג'", callback_data="זבחים דף צג'"),
#          [InlineKeyboardButton("זבחים דף צד'", callback_data="זבחים דף צד'")]],
#         [InlineKeyboardButton("זבחים דף צה'", callback_data="זבחים דף צה'"),
#          [InlineKeyboardButton("זבחים דף צו'", callback_data="זבחים דף צו'")]],
#         [InlineKeyboardButton("זבחים דף צז'", callback_data="זבחים דף צז'"),
#          [InlineKeyboardButton("זבחים דף צח'", callback_data="זבחים דף צח'")]],
#         [InlineKeyboardButton("זבחים דף צט'", callback_data="זבחים דף צט'"),
#          [InlineKeyboardButton("זבחים דף ק'", callback_data="זבחים דף ק'")]],
#         [InlineKeyboardButton("זבחים דף קא'", callback_data="זבחים דף קא'"),
#          [InlineKeyboardButton("זבחים דף קב'", callback_data="זבחים דף קב'")]],
#         [InlineKeyboardButton("זבחים דף קג'", callback_data="זבחים דף קג'"),
#          [InlineKeyboardButton("זבחים דף קד'", callback_data="זבחים דף קד'")]],
#         [InlineKeyboardButton("זבחים דף קה'", callback_data="זבחים דף קה'"),
#          [InlineKeyboardButton("זבחים דף קו'", callback_data="זבחים דף קו'")]],
#         [InlineKeyboardButton("זבחים דף קז'", callback_data="זבחים דף קז'"),
#          [InlineKeyboardButton("זבחים דף קח'", callback_data="זבחים דף קח'")]],
#         [InlineKeyboardButton("זבחים דף קט'", callback_data="זבחים דף קט'"),
#          [InlineKeyboardButton("זבחים דף קי'", callback_data="זבחים דף קי'")]],
#         [InlineKeyboardButton("זבחים דף קיא'", callback_data="זבחים דף קיא'"),
#          [InlineKeyboardButton("זבחים דף קיב'", callback_data="זבחים דף קיב'")]],
#         [InlineKeyboardButton("זבחים דף קיג'", callback_data="זבחים דף קיג'"),
#          [InlineKeyboardButton("זבחים דף קיד'", callback_data="זבחים דף קיד'")]],
#         [InlineKeyboardButton("זבחים דף קטו'", callback_data="זבחים דף קטו'"),
#          [InlineKeyboardButton("זבחים דף קטז'", callback_data="זבחים דף קטז'")]],
#         [InlineKeyboardButton("זבחים דף קיח'", callback_data="זבחים דף קיח'"),
#          [InlineKeyboardButton("זבחים דף קיט'", callback_data="זבחים דף קיט'")]],
#         [InlineKeyboardButton("זבחים דף קכ'", callback_data="זבחים דף קכ'"),
#         [InlineKeyboardButton("חזרה לבחירת סדר", callback_data='back')]]
#     ]
#     return InlineKeyboardMarkup(keyboard)

def zevachim_menu(bot, update):
    kb = [[InlineKeyboardButton("זבחים דפים ב-ס", callback_data='part זבחים א')],
          [InlineKeyboardButton("זבחים דפים סא-קכ", callback_data='חלק זבחים ב')],
          [InlineKeyboardButton("חזרה לבחירת סדר", callback_data='back')]]
    query = update.callback_query
    bot.edit_message_text(chat_id=query.message.chat_id,
                          message_id=query.message.message_id,
                          text="בחר חלק:",
                          reply_markup=InlineKeyboardMarkup(kb))


def menachot_menu(bot, update):
    kb = [[InlineKeyboardButton("מנחות דפים ב-ס", callback_data='part מנחות א')],
          [InlineKeyboardButton("מנחות דפים סא-קי", callback_data='חלק מנחות ב')],
          [InlineKeyboardButton("חזרה לבחירת סדר", callback_data='back')]]
    query = update.callback_query
    bot.edit_message_text(chat_id=query.message.chat_id,
                          message_id=query.message.message_id,
                          text="בחר חלק:",
                          reply_markup=InlineKeyboardMarkup(kb))


def zevachim_menu_a(bot, update):
    # print(dir(update.callback_query))
    # print(update.callback_query)
    # print(f'data = {update.callback_query["data"]}')
    # print(f'text = {update.callback_query.message["text"]}')
    query = update.callback_query
    bot.edit_message_text(chat_id=query.message.chat_id,
                          message_id=query.message.message_id,
                          text=links_to_files.daf_menu_message(),
                          reply_markup=links_to_files.zvachim_dapim_a())


def zevachim_menu_b(bot, update):
    # print(dir(update.callback_query))
    # print(update.callback_query)
    # print(update.callback_query["data"])
    # print(update.callback_query.message["text"])
    query = update.callback_query
    bot.edit_message_text(chat_id=query.message.chat_id,
                          message_id=query.message.message_id,
                          text=links_to_files.daf_menu_message(),
                          reply_markup=links_to_files.zvachim_dapim_b())


def menachot_menu_a(bot, update):
    # print(dir(update.callback_query))
    # print(update.callback_query)
    # print(f'data = {update.callback_query["data"]}')
    # print(f'text = {update.callback_query.message["text"]}')
    query = update.callback_query
    bot.edit_message_text(chat_id=query.message.chat_id,
                          message_id=query.message.message_id,
                          text=links_to_files.daf_menu_message(),
                          reply_markup=links_to_files.menachot_dapim_a())


def menachot_menu_b(bot, update):
    # print(dir(update.callback_query))
    # print(update.callback_query)
    # print(update.callback_query["data"])
    # print(update.callback_query.message["text"])
    query = update.callback_query
    bot.edit_message_text(chat_id=query.message.chat_id,
                          message_id=query.message.message_id,
                          text=links_to_files.daf_menu_message(),
                          reply_markup=links_to_files.menachot_dapim_b())


def main():
    updater = Updater(token=TOKEN)
    dispatcher = updater.dispatcher

    dispatcher.add_handler(CommandHandler('start', start))
    dispatcher.add_handler(CallbackQueryHandler(main_menu, pattern='back'))
    dispatcher.add_handler(CallbackQueryHandler(zeraim_menu, pattern='זרעים'))
    dispatcher.add_handler(CallbackQueryHandler(moeid_menu, pattern='מועד'))
    dispatcher.add_handler(CallbackQueryHandler(nashim_menu, pattern='נשים'))
    dispatcher.add_handler(CallbackQueryHandler(nezikin_menu, pattern='נזיקין'))
    dispatcher.add_handler(CallbackQueryHandler(kodshim_menu, pattern='קדשים'))
    dispatcher.add_handler(CallbackQueryHandler(teharot_menu, pattern='טהרות'))
    dispatcher.add_handler(CallbackQueryHandler(zevachim_menu, pattern='זבחים'))
    dispatcher.add_handler(CallbackQueryHandler(menachot_menu, pattern='מנחות'))
    dispatcher.add_handler(CallbackQueryHandler(zevachim_menu_a, pattern='part זבחים א'))
    dispatcher.add_handler(CallbackQueryHandler(zevachim_menu_b, pattern='חלק זבחים ב'))
    dispatcher.add_handler(CallbackQueryHandler(menachot_menu_a, pattern='part מנחות א'))
    dispatcher.add_handler(CallbackQueryHandler(menachot_menu_b, pattern='חלק מנחות ב'))

    # dispatcher.add_handler(CallbackQueryHandler(second_menu, pattern='m2'))
    # dispatcher.add_handler(CallbackQueryHandler(theerd_menu, pattern='m3'))
    # dispatcher.add_handler(CallbackQueryHandler(first_submenu,
    #                                             pattern='m1_1'))
    # updater.dispatcher.add_handler(CallbackQueryHandler(second_submenu,
    #                                                     pattern='m2_1'))

    # updater.dispatcher.add_handler(CallbackQueryHandler(button))
    # dispatcher.add_handler(CommandHandler('start', start))
    # dispatcher.add_error_handler(error)
    # # dispatcher.add_handler(CommandHandler('help', help))
    #
    dispatcher.add_handler(MessageHandler(Filters.text, echo))
    #
    # dispatcher.add_handler(MessageHandler(Filters.command, unknown))

    updater.start_polling()
    updater.idle()
    # updater.stop()


if __name__ == '__main__':
    main()


# conv_handler = ConversationHandler(
    #     entry_points=[CommandHandler('start', start)],
    #     states={
    #         SEDER: [CallbackQueryHandler(seder_keyboard)],
    #         MASECHET: [CallbackQueryHandler(masechet_keyboard)],
    #         PEREK: [CallbackQueryHandler(masechet_keyboard)]
    #     },
    #     fallbacks=[CommandHandler('start', start)]
    # )
